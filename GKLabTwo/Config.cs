﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GKLabTwo
{
    public static class Config
    {
        //public const int BitmapSize = 942;
        public static readonly int PenWidth = 1;
        public static readonly int FontSize = 6;
        public static readonly string FontName = "Arial";

        public static readonly bool RefreshAfterEachLine = false;
        public static readonly bool BigMamaTriangle = true;

        public static readonly float FanSpeed = 0.6f;
        public static readonly int FanFlatDistance = 100;
        public static readonly float FanHeight = 100;
        public static readonly float FanLightSpeed = 50 / 255f;


        public static readonly float LightSpeed = 0.4f;
        public static readonly int LightFlatDistance = 200;
        public static readonly float LightHeight = 200;

        public static readonly Color FillColor = Color.FromKnownColor(KnownColor.Red);
        public static readonly Color EdgeColor = Color.FromKnownColor(KnownColor.Gray);
        public static readonly Color LabelColor = Color.FromKnownColor(KnownColor.Black);

    }
}
