﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GKLabTwo.Models
{
    public class Edge
    {
        public Vertex Start { get; set; }
        public Point V1 => Start.Location;

        public Vertex End { get; set; }
        public Point V2 => End.Location;

        public string Name { get; private set; }

        public Edge()
        {
            Name = new Random().Next(0, 999).ToString();
        }
    }
}
