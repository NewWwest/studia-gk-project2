﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GKLabTwo.Models
{
    class FanLight
    {
        public Vector Color1 { get; set; }
        public Vector Location1 { get; set; }

        public Vector Color2 { get; set; }
        public Vector Location2 { get; set; }

        public Vector Color3 { get; set; }
        public Vector Location3 { get; set; }
    }
}
