﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GKLabTwo.Models
{
    class Square : Polygon
    {
        List<Vertex> vertecies = new List<Vertex>();
        List<Edge> edges = new List<Edge>();

        public override List<Edge> Edges => edges;
        public override List<Vertex> Vertecies => vertecies;

        private Vertex LastVertex => vertecies.Last();
        private Vertex AlmostLastVertex => vertecies[vertecies.Count - 2];
        private Vertex FirstVertex => vertecies.First();

        public Square()
        {
            var p1 = new Point(50, 50);
            var p2 = new Point(50, 500);
            var p3 = new Point(500, 500);
            var p4 = new Point(500,50);
            var v1 = new Vertex() { Location = p1 };
            var v2 = new Vertex() { Location = p2 };
            var v3 = new Vertex() { Location = p3 };
            var v4 = new Vertex() { Location = p4 };
            var e1 = new Edge() { Start = v1, End = v2 };
            var e2 = new Edge() { Start = v2, End = v3 };
            var e3 = new Edge() { Start = v3, End = v4 };
            var e4 = new Edge() { Start = v4, End = v1 };
            vertecies.Add(v1);
            edges.Add(e1);
            vertecies.Add(v2);
            edges.Add(e2);
            vertecies.Add(v3);
            edges.Add(e3);
            vertecies.Add(v4);
            edges.Add(e4);
            v1.Forward = e1;
            v1.Back = e4;
            v2.Forward = e2;
            v2.Back = e1;
            v3.Forward = e3;
            v3.Back = e2;
            v4.Forward = e4;
            v4.Back = e1;
        }
    }
}
