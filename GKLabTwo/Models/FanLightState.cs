﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GKLabTwo.Models
{
    enum FanLightState
    {
        AddGreen,
        SubRed,
        AddBlue,
        SubGreen,
        AddRed,
        SubBlue
    }
}
